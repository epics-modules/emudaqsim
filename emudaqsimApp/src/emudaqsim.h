/* emudaqsim.h
 *
 * This is a driver that emulates a DAQ hardware.
 * Based on ADSIS8300 from Hinko Kocevar (ESS ERIC).
 *
 */

#ifndef _EMUDAQSIM_H_
#define _EMUDAQSIM_H_

#include <stdint.h>
#include <epicsEvent.h>
#include <epicsTime.h>

#include <asynNDArrayDriver.h>

/* Compilation parameters */
#define ADC_MAX_NSAMPLES 100000
#define ADC_RESOLUTION 16
#define ADC_MAXVOLT 10.0
#define ADC_FREQUENCY 5000000.0
#define ADC_CLK_INTERNAL 0
#define CYCLE_PERIOD 71.7285e-3

/* asyn parameters strings */
#define DAQNumSamples_                 "DAQ.NUM_SAMPLES"
#define DAQTrigDelay_                  "DAQ.TRIG_DELAY"

#define DAQSamplingRate_               "DAQ.SAMPLING_RATE"
#define DAQDecimation_                 "DAQ.DECIMATION"
#define DAQAveraging_                  "DAQ.AVERAGING"
#define DAQClockDiv_                   "DAQ.CLOCK_DIV"
#define DAQClockFreq_                  "DAQ.CLOCK_FREQ"
#define DAQTickValue_                  "DAQ.TICK_VALUE"

#define DAQClockSource_                "DAQ.CLOCK_SOURCE"

#define DAQTrigSource_                 "DAQ.TRIG_SOURCE"
#define DAQTrigEdge_                   "DAQ.TRIG_EDGE"
#define DAQTrigRepeat_                 "DAQ.TRIG_REPEAT"
#define DAQTrigThreshold_              "DAQ.TRIG_THRES"

#define DAQReset_                      "DAQ.RESET"
#define DAQMessage_                    "DAQ.MESSAGE"

#define DAQAPPSinature_                "DAQ.APP_SIGN"
#define DAQFWSignature_                "DAQ.FW_SIGN"
#define DAQDeviceType_                 "DAQ.DEVICE_TYPE"
#define DAQDeviceTypeStr_              "DAQ.DEVICE_TYPE_STR"

#define DAQCHCtrlStat_                 "DAQ.CH.CTRL_STAT"
#define DAQConvGain_                   "DAQ.CH.CONV_GAIN"
#define DAQConvOffset_                 "DAQ.CH.CONV_OFFS"
#define DAQChannelBgSubCtrlStat_       "DAQ.CH.BGSUB.CTRLSTAT"
#define DAQChannelBgSubZero_           "DAQ.CH.BGSUB.ZERO"
#define DAQChannelBgSubSize_           "DAQ.CH.BGSUB.SIZE"
#define DAQChannelBgSubValue_          "DAQ.CH.BGSUB.VALUE"

#define DAQChGenWaitTime_              "DAQ.CH.GEN.WAIT_TIME"
#define DAQChGenPulseDelay_            "DAQ.CH.GEN.PULSE.DELAY"
#define DAQChGenPulseHeight_           "DAQ.CH.GEN.PULSE.HEIGHT"
#define DAQChGenPulseWidth_            "DAQ.CH.GEN.PULSE.WIDTH"
#define DAQChGenPulseOffset_           "DAQ.CH.GEN.PULSE.OFFSET"
#define DAQChGenPulseNoise_            "DAQ.CH.GEN.PULSE.NOISE"

#define MAX_PATH_LEN                   32
#define MAX_LOG_STR_LEN                256


/* log functionality */
#define DAQ_LOG(p, s, t, v) ({\
    snprintf(mDaqLogStr, MAX_LOG_STR_LEN, "%s %s::%s: %s", p, driverName, __func__, s); \
    asynPrint(pasynUserSelf, t, "%s\n", mDaqLogStr); \
    setStringParam(mDaqMessage, mDaqLogStr); \
    if (v) { \
        printf("%s\n", mDaqLogStr); \
    } \
})

#define DAQ_ERROR(s)    ({ DAQ_LOG("[ERR]", s, ASYN_TRACE_ERROR, 1); })
#define DAQ_FLOW(s)     ({ DAQ_LOG("[LOG]", s, ASYN_TRACE_FLOW, 0); })
#define DAQ_WARN(s)     ({ DAQ_LOG("[WRN]", s, ASYN_TRACE_WARNING, 0); })


#ifdef FAKEIFCDEV
#define IFCDAQDRV_CALL_0(s, x) ({\
    int __ret = 0; \
    __ret; \
})
#else
#define IFCDAQDRV_CALL_0(s, x) ({\
    int __ret = x; \
    if (__ret) { DAQ_ERROR(s); } \
    __ret; \
})
#endif

#define IFCDAQDRV_CALL(s, x) ({\
    int __ret = IFCDAQDRV_CALL_0(s, x); \
    __ret; \
})

#define IFCDAQDRV_CALL_VOID(s, x) ({ IFCDAQDRV_CALL_0(s, x); })

#define IFCDAQDRV_CALL_RET(s, x) ({\
    int __ret = IFCDAQDRV_CALL_0(s, x); \
    if (__ret) { return __ret; } \
})

#if _DBG == 1
    #undef D
    #define D(x) { \
    printf("[DBG] %s::%s: ", driverName, __func__); \
    x; \
}
#else
    #undef D
    #define D(x)
#endif


/** EMU DAQ Simulator driver class **/
class epicsShareClass EmuDaqSim : public asynNDArrayDriver {
public:
    EmuDaqSim(const char *portName, const int maxChannels,
            int numSamples, int extraPorts, int maxBuffers, size_t maxMemory,
            int priority, int stackSize);
    virtual ~EmuDaqSim();

    /* These are the methods that we override from asynNDArrayDriver */
    virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
    virtual asynStatus writeFloat64(asynUser *pasynUser, epicsFloat64 value);
    virtual void report(FILE *fp, int details);
    /**< Should be private, but gets called from C, so must be public */
    void emusimTask();
    //void Sim14HzTask();

protected:
    // system
    int mDaqNumSamples;
    #define EMUDAQSIM_FIRST_PARAM mDaqNumSamples
    int mDaqTrigDelay;

    int mDaqSamplingRate;
    int mDaqDecimation;
    int mDaqAveraging;
    int mDaqClockDiv;
    int mDaqClockFreq;
    int mDaqTickValue;

    int mDaqClockSource;

    int mDaqTrigSource;
    int mDaqTrigEdge;
    int mDaqTrigRepeat;
    int mDaqTrigThres;

    int mDaqReset;
    int mDaqMessage;

    int mDaqAPPSinature;
    int mDaqFWSignature;
    int mDaqDeviceType;
    int mDaqDeviceTypeStr;

    // individual channel parameters
    int mCHCtrlStat;
    int mCHConvGain;
    int mCHConvOffset;
    int mCHBgSubCtrlStat;
    int mCHBgSubZero;
    int mCHBgSubSize;
    int mCHBgSubValue;
    
    // fake signal generator parameters
    int mCHGenWaitTime;
    int mCHGenPulseDelay;
    int mCHGenPulseHeight;
    int mCHGenPulseWidth;
    int mCHGenPulseOffset;
    int mCHGenPulseNoise;


    /* These are the methods that are new to this class */
    virtual int acquireRawArrays();
    void setAcquire(int value);
    virtual int initDevice();
    virtual int destroyDevice();
    virtual int initDeviceDone();
    virtual int armDevice();
    virtual int disarmDevice();
    virtual int waitForDevice(double processtime);
    virtual int deviceDone();
    virtual int updateParameters();
    virtual int refreshParameters();

    uint64_t mChannelMask;
    uint32_t mNumChannels;
    char mDaqLogStr[MAX_LOG_STR_LEN];
    NDArray *mRawDataArray;
    int mNumArrays;

private:

    /* Our data */
    epicsEventId startEventId_;
    epicsEventId stopEventId_;
    epicsEventId cycleEventId_;
    int uniqueId_;
    int acquiring_;
    double elapsedTime_;

    bool mDoNumSamplesUpdate;
    bool mDoTriggerUpdate;
    bool mDoTriggerThresUpdate;
    bool mDoClockSrcUpdate;
    bool mDoClockFreqUpdate;
    bool mDoClockDivUpdate;
    bool mDoDecimationUpdate;
    bool mDoAveragingUpdate;
    bool mDoSamplingRateUpdate;
    bool mDoChannelMaskUpdate;

};



#endif /* _EMUDAQSIM_H_ */
