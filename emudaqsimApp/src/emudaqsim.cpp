/* emudaqsim.cpp
 *
 * This is a driver that emulates a DAQ hardware.
 * Based on ADSIS8300 from Hinko Kocevar (ESS ERIC).
 *
 */


#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <assert.h>
#include <string>
#include <chrono>
#include <thread>

#include <dbAccess.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsEvent.h>
#include <epicsExit.h>
#include <iocsh.h>

#include <asynNDArrayDriver.h>
#include <epicsExport.h>

#include "emudaqsim.h"

static const char *driverName = "emudaqsim";

/**
 * Exit handler, delete the EmuDaqSim object.
 */
static void exitHandler(void *drvPvt) {
    EmuDaqSim *pPvt = (EmuDaqSim *) drvPvt;
    delete pPvt;
}

static void emusimTaskC(void *drvPvt)
{
    EmuDaqSim *pPvt = (EmuDaqSim *)drvPvt;
    pPvt->emusimTask();
}

/*
 * Round upwards to nearest power-of-2.
 */

static inline int ceil_pow2(unsigned number) {
    unsigned n = 1;
    unsigned i = number - 1;
    while(i) {
        n <<= 1;
        i >>= 1;
    }
    return n;
}

/*
 * Round upwards to nearest mibibytes
 */

// static inline int ceil_mibi(unsigned number) {
//     return (1 + (number-1) / (1024 * 1024)) * 1024 * 1024;
// }


/** Constructor for EmuDaqSim; most parameters are simply passed to asynNDArrayDriver::asynNDArrayDriver.
  * After calling the base class constructor this method creates a thread to compute the simulated detector data,
  * and sets reasonable default values for parameters defined in this class, asynNDArrayDriver and ADDriver.
  * \param[in] portName The name of the asyn port driver to be created.
  * \param[in] maxChannels Max number of ADC channels to be acquired.
  * \param[in] numSamples The initial number of AI samples.
  * \param[in] extraPorts Additinonal number of asyn ports to register. Used for class that extend this class.
  * \param[in] maxBuffers The maximum number of NDArray buffers that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited number of buffers.
  * \param[in] maxMemory The maximum amount of memory that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited amount of memory.
  * \param[in] priority The thread priority for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  * \param[in] stackSize The stack size for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  */
EmuDaqSim::EmuDaqSim(const char *portName, const int maxChannels,
            int numSamples, int extraPorts, int maxBuffers, size_t maxMemory,
            int priority, int stackSize)

    : asynNDArrayDriver(portName,
            maxChannels + extraPorts,
            maxBuffers, maxMemory,
            asynFloat64ArrayMask | asynUInt32DigitalMask,
            asynFloat64ArrayMask | asynUInt32DigitalMask,
            ASYN_CANBLOCK | ASYN_MULTIDEVICE, /* asyn flags*/
            1,                                /* autoConnect=1 */
            priority,
            stackSize),
            uniqueId_(0), acquiring_(0)

{

    int status = asynSuccess;
    D(printf("driver has %d asyn addresses\n", maxAddr));

    mRawDataArray = NULL;
    mNumArrays = maxChannels;
    mNumChannels = maxChannels;

    /* Create an EPICS exit handler */
    epicsAtExit(exitHandler, this);

    /* Create the epicsEvents for signaling to the acquisition
     * task when acquisition starts and stops */
    this->startEventId_ = epicsEventCreate(epicsEventEmpty);
    if (!this->startEventId_) {
        DAQ_ERROR("epicsEventCreate failure for start event\n");
        return;
    }
    
    this->stopEventId_ = epicsEventCreate(epicsEventEmpty);
    if (!this->stopEventId_) {
        DAQ_ERROR("epicsEventCreate failure for stop event\n");
        return;
    }

    this->cycleEventId_ = epicsEventCreate(epicsEventEmpty);
    if (!this->cycleEventId_) {
        DAQ_ERROR("epicsEventCreate failure for cycle event\n");
        return;
    }

    // system parameters
    createParam(DAQNumSamples_,            asynParamInt32, &mDaqNumSamples);
    createParam(DAQTrigDelay_,             asynParamInt32, &mDaqTrigDelay);

    createParam(DAQSamplingRate_,          asynParamFloat64, &mDaqSamplingRate);
    createParam(DAQDecimation_,            asynParamInt32, &mDaqDecimation);
    createParam(DAQAveraging_,             asynParamInt32, &mDaqAveraging);
    createParam(DAQClockDiv_,              asynParamInt32, &mDaqClockDiv);
    createParam(DAQClockFreq_,             asynParamFloat64, &mDaqClockFreq);
    createParam(DAQTickValue_,             asynParamFloat64, &mDaqTickValue);

    createParam(DAQClockSource_,           asynParamInt32, &mDaqClockSource);

    createParam(DAQTrigSource_,            asynParamInt32, &mDaqTrigSource);
    createParam(DAQTrigEdge_,              asynParamInt32, &mDaqTrigEdge);
    createParam(DAQTrigRepeat_,            asynParamInt32, &mDaqTrigRepeat);
    createParam(DAQTrigThreshold_,         asynParamFloat64, &mDaqTrigThres);

    createParam(DAQReset_,                 asynParamInt32, &mDaqReset);
    createParam(DAQMessage_,               asynParamOctet, &mDaqMessage);

    createParam(DAQAPPSinature_,           asynParamInt32, &mDaqAPPSinature);
    createParam(DAQFWSignature_,           asynParamInt32, &mDaqFWSignature);
    createParam(DAQDeviceType_,            asynParamInt32, &mDaqDeviceType);
    createParam(DAQDeviceTypeStr_,         asynParamOctet, &mDaqDeviceTypeStr);

    // Individual channel parameters
    createParam(DAQCHCtrlStat_,            asynParamInt32,   &mCHCtrlStat); 
    createParam(DAQConvGain_,              asynParamFloat64, &mCHConvGain);
    createParam(DAQConvOffset_,            asynParamFloat64, &mCHConvOffset);
    createParam(DAQChannelBgSubCtrlStat_,  asynParamInt32,   &mCHBgSubCtrlStat);
    createParam(DAQChannelBgSubZero_,      asynParamFloat64, &mCHBgSubZero);
    createParam(DAQChannelBgSubSize_,      asynParamInt32,   &mCHBgSubSize);
    createParam(DAQChannelBgSubValue_,     asynParamFloat64, &mCHBgSubValue);

    // fake signal generator
    createParam(DAQChGenWaitTime_,         asynParamFloat64, &mCHGenWaitTime);
    createParam(DAQChGenPulseDelay_,       asynParamFloat64, &mCHGenPulseDelay);
    createParam(DAQChGenPulseHeight_,      asynParamFloat64, &mCHGenPulseHeight);
    createParam(DAQChGenPulseWidth_,       asynParamFloat64, &mCHGenPulseWidth);
    createParam(DAQChGenPulseOffset_,      asynParamFloat64, &mCHGenPulseOffset);
    createParam(DAQChGenPulseNoise_,       asynParamFloat64, &mCHGenPulseNoise);

    // this is digitizer native sample format
    setIntegerParam(NDDataType, NDUInt16);

    setIntegerParam(mDaqAPPSinature, 0);
    setIntegerParam(mDaqFWSignature, 0);
    setIntegerParam(mDaqDeviceType, 0);
    setStringParam(mDaqDeviceTypeStr, "Unknown");
    setDoubleParam(mDaqTickValue, 1.0);

    DAQ_FLOW("No error");

    /* Create the thread that updates the data */
    status = (epicsThreadCreate("DAQTask",
                                epicsThreadPriorityHigh,
                                epicsThreadGetStackSize(epicsThreadStackMedium),
                                (EPICSTHREADFUNC)emusimTaskC,
                                this) == NULL);
    if (status) {
        DAQ_ERROR("epicsThreadCreate failure for acquisition task\n");
        return;
    }

    this->lock();
    initDevice();
    this->unlock();

    // maximum number of samples we can ask for, per channel
    const uint32_t maxSamples = ADC_MAX_NSAMPLES;
    if (numSamples > (int) maxSamples) 
        numSamples = maxSamples;
    setIntegerParam(mDaqNumSamples, numSamples);

    /* Configure initial gain/offset conversion params */

#if 0
    const uint32_t resolution = ADC_RESOLUTION;
    const double vref_max = ADC_MAXVOLT;

    double linConvGain = vref_max/((1 << (resolution - 1)) - 1);
    /* Iterate over all channels to set initial values */
    for (int i = 0; i < (maxChannels + extraPorts); i++) {
        setDoubleParam(i, mCHConvGain, linConvGain);
        setDoubleParam(i, mCHConvOffset, 0.0);
    }
#else
    for (int i = 0; i < (maxChannels + extraPorts); i++) {
        setDoubleParam(i, mCHConvGain, 1.0);
        setDoubleParam(i, mCHConvOffset, 0.0);
    }
#endif

}

EmuDaqSim::~EmuDaqSim() {
    DAQ_FLOW("Shutdown and freeing up memory...\n");

    this->lock();
    DAQ_FLOW("Data thread is already down!\n");
    destroyDevice();
    this->unlock();
    DAQ_FLOW("Shutdown complete!\n");
}

int EmuDaqSim::initDevice()
{
    unsigned int deviceType = 0x3117;
    unsigned int APPSinature = 0x00;
    unsigned int FWSignature = 0x00;
    char deviceTypeStr[256];

    setIntegerParam(mDaqAPPSinature, APPSinature);
    setIntegerParam(mDaqFWSignature, FWSignature);
    setIntegerParam(mDaqDeviceType, deviceType);
    setStringParam(mDaqDeviceTypeStr, deviceTypeStr);

    /* Get sampling rate and clock parameters */
    const int pClkDiv = 1;
    const double pClkFreq = ADC_FREQUENCY;
    const int clkSource = ADC_CLK_INTERNAL;
    const double pSamplingRate = ADC_FREQUENCY;

    setIntegerParam(mDaqClockDiv, pClkDiv);
    setDoubleParam(mDaqClockFreq, pClkFreq);
    setDoubleParam(mDaqSamplingRate, pSamplingRate);
    setIntegerParam(mDaqClockSource, clkSource);

    callParamCallbacks(0);
    DAQ_FLOW("Initialization of fake card is done!\n");

    return 0;
}

int EmuDaqSim::destroyDevice()
{
    return 0;
}

int EmuDaqSim::initDeviceDone()
{
    DAQ_FLOW("Enter\n");
    return 0;
}

int EmuDaqSim::armDevice()
{
    DAQ_FLOW("Enter\n");
    return 0;
}

int EmuDaqSim::disarmDevice()
{
    DAQ_FLOW("Enter\n");
    return 0;
}

int EmuDaqSim::waitForDevice(double processtime)
{
    DAQ_FLOW("Enter\n");
    
    /* put the thread to sleep approx. to CYCLE PERIOD */
    if (processtime < CYCLE_PERIOD) {
        const int sleeptime = static_cast<int>((CYCLE_PERIOD - processtime)*1000.0);
        std::this_thread::sleep_for(std::chrono::milliseconds(sleeptime));
    }

    return 0;
}

int EmuDaqSim::deviceDone()
{
    DAQ_FLOW("Enter\n");
    return 0;
}

int EmuDaqSim::refreshParameters()
{
    DAQ_FLOW("Enter\n");
    return 0;
}


int EmuDaqSim::acquireRawArrays()
{
    size_t dims[1]; // One NDArray of one dimension per channel
    int numAiSamples;
    uint32_t aich;
    epicsFloat32 *pRaw;

    DAQ_FLOW("Enter\n");

    getIntegerParam(mDaqNumSamples, &numAiSamples);
    dims[0] = numAiSamples;

    /* Driver reads data according to the size of the buffer - prevent seg fault*/
    pRaw = (epicsFloat32 *)calloc(ADC_MAX_NSAMPLES, sizeof(epicsFloat32));

    double tickValue;
    getDoubleParam(mDaqTickValue, &tickValue);
    if (tickValue <= 0.0) {
        tickValue = 1.0;
    }

    for (aich = 0; aich < mNumChannels; aich++) {
        if (!(mChannelMask & (1 << aich))) {
            continue;
        }

        /* Release the lock of the particular NDArray */
        if (this->pArrays[aich]) {
            this->pArrays[aich]->release();
        }
        this->pArrays[aich] = NULL;

        /* Allocate NDArray */
        this->pArrays[aich] = pNDArrayPool->alloc(1, dims, NDFloat32, 0, 0);
        epicsFloat32 *pData = (epicsFloat32 *)this->pArrays[aich]->pData;

        double genPulseDelay;
        double genPulseHeight;
        double genPulseWidth;
        double genPulseOffset;
        double genPulseNoise;
        getDoubleParam(aich, mCHGenPulseDelay, &genPulseDelay);
        getDoubleParam(aich, mCHGenPulseHeight, &genPulseHeight);
        getDoubleParam(aich, mCHGenPulseWidth, &genPulseWidth);
        getDoubleParam(aich, mCHGenPulseOffset, &genPulseOffset);
        getDoubleParam(aich, mCHGenPulseNoise, &genPulseNoise);
        double elem;
        struct timespec tsnow;
        clock_gettime(CLOCK_MONOTONIC, &tsnow);
        srand(tsnow.tv_nsec);

        /* Convert Pulse Delay and Pulse Width from milliseconds to ticks */
        int genPulseDelayInt = genPulseDelay / (tickValue*1000);
        int genPulseWidthInt = genPulseWidth / (tickValue*1000);

        for (int i = 0; i < numAiSamples; i++) {
            // start with blank sample
            elem = 0.0;
            // add noise
            if (genPulseNoise) {
                // rand() range 0 to RAND_MAX, scale to noise level
                elem += (rand() % (int)genPulseNoise);
            }
            // add offset
            elem += genPulseOffset;
            // add pulse height if inside the pulse area
            if (i > genPulseDelayInt && i < (genPulseDelayInt + genPulseWidthInt)) {
                elem += genPulseHeight;
            }
            pRaw[i] = (epicsFloat32)elem;
        }

        /* Apply scaling conversion, background subtraction and transfer from pRaw to the NDArray data*/
        int bgSubCtrlStat;
        double gain, offset;
        getDoubleParam(aich, mCHConvGain, &gain);
        getDoubleParam(aich, mCHConvOffset, &offset);

        // Scaling (from ADC counts to EGU according to gain and offset from user)
        for (int samp = 0; samp < numAiSamples; samp++) {
            pData[samp] = pRaw[samp]*gain + offset;
        }

        // Background subtraction; TODO: check the float types (64 or 32 bits)
        getIntegerParam(mCHBgSubCtrlStat, &bgSubCtrlStat);
        if (bgSubCtrlStat) {
            int bgSubSize; 
            double bgSubZero;
            getDoubleParam(aich, mCHBgSubZero, &bgSubZero);
            getIntegerParam(aich, mCHBgSubSize, &bgSubSize);
            // compute the average value from samples within bg. sub. size parameter
            double bgSubValue = 0.0;
            int sz;
            for (sz = 0; sz < bgSubSize && sz < numAiSamples; sz++) {
                bgSubValue += pData[sz];
            }
            bgSubValue /= (double) sz;
            bgSubValue = bgSubValue - bgSubZero;
            setIntegerParam(aich, mCHBgSubSize, sz);
            setDoubleParam(aich, mCHBgSubValue, bgSubValue);

            for (int j = 0; j < numAiSamples; j++) {
                // remove the background value from all samples
                pData[j] = pData[j] - (epicsFloat32) bgSubValue;
            }

        }
    }

    // free pRaw
    free(pRaw);

    return 0;
}

void EmuDaqSim::setAcquire(int value)
{
    if (value && !acquiring_) {
        /* Send an event to wake up the simulation task */
        epicsEventSignal(this->startEventId_);
    }
    if (!value && acquiring_) {
        /* This was a command to stop acquisition */
        /* Send the stop event */
        disarmDevice();
        epicsEventSignal(this->stopEventId_);
    }
}

void EmuDaqSim::emusimTask()
{
    while (! interruptAccept) {
        epicsThreadSleep(0.1);
    }

    int status = asynSuccess;
    NDArray *pData;
    epicsTimeStamp frameTime;
    int arrayCounter;
    int i, a;
    int trgRepeat;
    int trgCount;
    int ret;
    epicsTimeStamp tEnd, tStart;

    /* Initialize elapsed time measurement */
    epicsTimeGetCurrent(&tStart);
    epicsTimeGetCurrent(&tEnd);

    sleep(1);

    this->lock();

    trgCount = 0;

    // loop forever
    while (1) {

taskStart:
        // update the parameters from user, before doing any setup
        ret = updateParameters();
        if (ret) {
            acquiring_ = 0;
            setIntegerParam(ADAcquire, 0);
        }

        D(printf("0 update user parameters\n"));
        // notify clients of the updates
        for (a = 0; a < maxAddr; a++) {
            callParamCallbacks(a);
        }

        D(printf("1 check acq\n"));

        // acquisition might have been stopped by the user
        status = epicsEventTryWait(this->stopEventId_);
        if (status == epicsEventWaitOK) {
            D(printf("1a stop event detected\n"));
            trgCount = 0;
            acquiring_ = 0;
        }

        // if we are not acquiring then wait
        if (! acquiring_) {
            D(printf("1a disarm device\n"));
            disarmDevice();
            callParamCallbacks(0);

            D(printf("1b wait for start event..\n"));
            // release the lock while we wait for the start event
            this->unlock();
            status = epicsEventWait(this->startEventId_);
            this->lock();
            D(printf("1c start event arrived!\n"));

            acquiring_ = 1;
            elapsedTime_ = 0.0;
            trgCount = 0;

            // go the the start of the loop in order to apply any new
            // parameters user might have set while acqusition was disabled
            goto taskStart;
        }

        D(printf("2a arm device!\n"));
        ret = armDevice();
        if (ret) {
            acquiring_ = 0;
            setIntegerParam(ADAcquire, 0);
            D(printf("Failed to arm device!\n"));
            goto taskStart;
        }
        callParamCallbacks(0);

        // unlock while waiting for the device
        this->unlock();

        D(printf("2b wait for acq to finish!\n"));
        ret = waitForDevice(epicsTimeDiffInSeconds(&tEnd, &tStart)); // Wait exactly to complete 14 Hz
        if (ret) {
            // lock it back
            this->lock();
            acquiring_ = 0;
            setIntegerParam(ADAcquire, 0);
            goto taskStart;
        }
        // lock it back
        this->lock();

        D(printf("2c acq finished!\n"));
        ret = deviceDone();
        if (ret) {
            acquiring_ = 0;
            setIntegerParam(ADAcquire, 0);
            goto taskStart;
        }

        // after the firmware acquired the data, some of the parameters
        // might need refreshing; i.e. register value changed -> update PV
        D(printf("2d refresh device parameters!\n"));
        ret = refreshParameters();
        if (ret) {
            acquiring_ = 0;
            setIntegerParam(ADAcquire, 0);
            goto taskStart;
        }
        callParamCallbacks(0);

        trgCount++;
        D(printf("2e read out channels..\n"));

        epicsTimeGetCurrent(&tStart);

        /* Get the data */
        ret = acquireRawArrays();
        if (ret) {
            acquiring_ = 0;
            setIntegerParam(ADAcquire, 0);
            goto taskStart;
        }
        D(printf("2e read out done!\n"));

        epicsTimeGetCurrent(&frameTime);
        getIntegerParam(NDArrayCounter, &arrayCounter);
        arrayCounter++;
        setIntegerParam(NDArrayCounter, arrayCounter);
        uniqueId_++;

        // notify clients of the new data arrays and time ticks for enabled
        // channels and AOIs
        for (a = 0; a < maxAddr; a++) {
            if (! this->pArrays[a]) {
                continue;
            }
            pData = this->pArrays[a];

            // set the frame number and time stamp
            pData->uniqueId = uniqueId_;
            pData->timeStamp = frameTime.secPastEpoch + frameTime.nsec / 1.e9;
            updateTimeStamp(&pData->epicsTS);

            // get any attributes that have been defined for this driver
            this->getAttributes(pData->pAttributeList);

            // must release the lock here, or we can get into a deadlock, because we can
            // block on the plugin lock, and the plugin can be calling us
            this->unlock();
            D(printf("3 doCallbacksGenericPointer for AI channel %d..\n", a));
            doCallbacksGenericPointer(pData, NDArrayData, a);
            this->lock();
        }

        getIntegerParam(mDaqTrigRepeat, &trgRepeat);
        D(printf("4 trigger repeat %d\n", trgRepeat));
        // stop the acq if trigger is not set to repeat,
        // or the trigger count has been reached
        if ((trgRepeat == 0) ||
            ((trgRepeat > 0) && (trgCount >= trgRepeat))) {
            D(printf("4a stop acq\n"));
            acquiring_ = 0;
            setIntegerParam(ADAcquire, 0);
        }

        /* Call the callbacks to update any changes */
        for (i=0; i<maxAddr; i++) {
            callParamCallbacks(i);
        }

        epicsTimeGetCurrent(&tEnd);
    }

    callParamCallbacks(0);
    DAQ_FLOW("Data thread is down!\n");
}


int EmuDaqSim::updateParameters()
{
	struct timespec now = {0, 0};
	clock_gettime(CLOCK_REALTIME, &now);

	uint32_t sample_size = 2;
	sample_size /= 8;

	/* Local variables for managing sampling rate related parameters */
	int pAveraging;
	int pDecimation;
	int pClkDiv;
	double pClkFreq;
	double pSamplingRate;
	getIntegerParam(mDaqAveraging, &pAveraging);
	getIntegerParam(mDaqDecimation, &pDecimation);
	getIntegerParam(mDaqClockDiv, &pClkDiv);
	getDoubleParam(mDaqClockFreq, &pClkFreq);
	getDoubleParam(mDaqSamplingRate, &pSamplingRate);


	if(mDoNumSamplesUpdate) {
		mDoNumSamplesUpdate = false;

        uint32_t curr_nSamples;
        getIntegerParam(mDaqNumSamples, (int *) &curr_nSamples);
        curr_nSamples = ceil_pow2(curr_nSamples);

        if (curr_nSamples > ADC_MAX_NSAMPLES)
            curr_nSamples = ADC_MAX_NSAMPLES;

        // Assert a valid number of samples
        setIntegerParam(mDaqNumSamples, (int) curr_nSamples);
	}

	if(mDoTriggerUpdate || mDoTriggerThresUpdate)
	{
		mDoTriggerUpdate = false;
		mDoTriggerThresUpdate = false;

		DAQ_FLOW("Updating TRIGGER parameter\n");

		/* Current parameters */
		int curr_TrigSource;
		int curr_TrigEdge;
		double curr_TrigThres;
		getIntegerParam(mDaqTrigSource, &curr_TrigSource);
		getIntegerParam(mDaqTrigEdge, &curr_TrigEdge);
		getDoubleParam(mDaqTrigThres, &curr_TrigThres);
	}

	if(mDoClockSrcUpdate) {
		mDoClockSrcUpdate = false;

		int clkSource;
		getIntegerParam(mDaqClockSource, &clkSource);
		DAQ_FLOW("Updating CLOCK SOURCE parameter\n");
		setIntegerParam(mDaqClockSource, 0);
	}

	if(mDoClockFreqUpdate) {
		mDoClockFreqUpdate = false;
		DAQ_FLOW("Updating CLOCK FREQ param\n");
		setDoubleParam(mDaqSamplingRate, ADC_FREQUENCY);
		setDoubleParam(mDaqClockFreq, ADC_FREQUENCY);
	}

	if(mDoClockDivUpdate) {
		mDoClockDivUpdate = false;
		DAQ_FLOW("Updating CLOCK DIV param\n");
		setDoubleParam(mDaqSamplingRate, ADC_FREQUENCY);
		setIntegerParam(mDaqClockDiv, 1);
	}

	if(mDoDecimationUpdate) {
		mDoDecimationUpdate = false;
		DAQ_FLOW("Updating DECIMATION parameter\n");
		setDoubleParam(mDaqSamplingRate, ADC_FREQUENCY);
		setIntegerParam(mDaqDecimation, 1);
	}

	if(mDoAveragingUpdate) {
		mDoAveragingUpdate = false;
		DAQ_FLOW("Updating AVERAGING parameter\n");
		setDoubleParam(mDaqSamplingRate, ADC_FREQUENCY);
		setIntegerParam(mDaqAveraging, 1);
	}

	if(mDoSamplingRateUpdate) {
		mDoSamplingRateUpdate = false;

		DAQ_FLOW("Updating SAMPLING RATE parameter\n");
		setIntegerParam(mDaqAveraging, 1);
		setIntegerParam(mDaqDecimation, 1);
		setIntegerParam(mDaqClockDiv, 1);
		setDoubleParam(mDaqClockFreq, ADC_FREQUENCY);
		setDoubleParam(mDaqSamplingRate, ADC_FREQUENCY);
	}

        /* Enable / disable channels. */
    if (mDoChannelMaskUpdate) {
        int tmp;
        for (uint32_t a = 0; a < mNumChannels; a++) {
            getIntegerParam(a, mCHCtrlStat, &tmp);
            if (tmp) {
                mChannelMask |= (1 << a);
            } else {
                mChannelMask &= ~(1 << a);
            }
        }

        mDoChannelMaskUpdate = false;
    }

    double samplingFreq;
    getDoubleParam(mDaqSamplingRate, &samplingFreq);
    double tick = 1.0 / samplingFreq;
    setDoubleParam(mDaqTickValue, tick);


	return 0;
}

/** Called when asyn clients call pasynInt32->write().
  * This function performs actions for some parameters.
  * For all parameters it sets the value in the parameter library and calls any registered callbacks..
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Value to write. */
asynStatus EmuDaqSim::writeInt32(asynUser *pasynUser, epicsInt32 value)
{
    int function = pasynUser->reason;
    int addr;
    int ret;
    const char *name;
    asynStatus status = asynSuccess;

    getAddress(pasynUser, &addr);
    getParamName(function, &name);

    /* Set the parameter and readback in the parameter library.  This may be overwritten when we read back the
     * status at the end, but that's OK */
    status = setIntegerParam(addr, function, value);

    if (function == ADAcquire) {
        setAcquire(value);
    }

    else if (function == mDaqNumSamples || function == mDaqTrigDelay) {
        mDoNumSamplesUpdate = true;
    }

    else if (function == mDaqTrigSource || function == mDaqTrigEdge) {
        mDoTriggerUpdate = true;
    }

    else if (function == mDaqClockSource) {
        mDoClockSrcUpdate = true;
    }

    else if (function == mDaqClockDiv) {
        mDoClockDivUpdate = true;
    }

    else if (function == mDaqDecimation) {
        mDoDecimationUpdate = true;
    }

    else if (function == mDaqAveraging) {
        mDoAveragingUpdate = true;
    }

    else if (function == mCHCtrlStat) {
        mDoChannelMaskUpdate = true;
    }

    /* If this parameter belongs to a base class call its method */
    if (function < EMUDAQSIM_FIRST_PARAM) {
        status = asynNDArrayDriver::writeInt32(pasynUser, value);
    }

    /* UPDATE parameters on the firmware */
    ret = updateParameters();
    if (ret) {
        status = asynError;
    }

    /* Do callbacks so higher layers see any changes */
    callParamCallbacks(addr);

    if (status)
        asynPrint(pasynUser, ASYN_TRACE_ERROR,
              "%s:writeInt32 error, status=%d function=%d, value=%d\n",
              driverName, status, function, value);
    else
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
              "%s:writeInt32: function=%d, value=%d\n",
              driverName, function, value);
    return status;
}


/** Called when asyn clients call pasynFloat64->write().
  * This function performs actions for some parameters.
  * For all parameters it sets the value in the parameter library and calls any registered callbacks..
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Value to write. */
asynStatus EmuDaqSim::writeFloat64(asynUser *pasynUser, epicsFloat64 value)
{
    int function = pasynUser->reason;
    int addr;
    int ret;
    const char *name;
    asynStatus status = asynSuccess;

    getAddress(pasynUser, &addr);
    getParamName(function, &name);

    /* Set the parameter and readback in the parameter library.  This may be overwritten when we read back the
     * status at the end, but that's OK */
    status = setDoubleParam(addr, function, value);

    /* If it's changing the sampling frequency */
    if (function == mDaqSamplingRate) {
        mDoSamplingRateUpdate = true;
    }

    /* If it's changing the trigger threashold */
    else if (function == mDaqTrigThres) {
        mDoTriggerThresUpdate = true;
    }

    /* If it's changing the clock frequency */
    else if (function == mDaqClockFreq) {
        mDoClockFreqUpdate = true;
    }


    /* If this parameter belongs to a base class call its method */
    if (function < EMUDAQSIM_FIRST_PARAM) {
        status = asynNDArrayDriver::writeFloat64(pasynUser, value);
    }

    /* UPDATE parameters on the firmware */
    ret = updateParameters();
    if (ret) {
        status = asynError;
    }

    /* Do callbacks so higher layers see any changes */
    callParamCallbacks(addr);

    if (status)
        asynPrint(pasynUser, ASYN_TRACE_ERROR,
              "%s:writeFloat64 error, status=%d function=%d, value=%f\n",
              driverName, status, function, value);
    else
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
              "%s:writeFloat64: function=%d, value=%f\n",
              driverName, function, value);
    return status;
}


/** Report status of the driver.
  * Prints details about the driver if details>0.
  * It then calls the ADDriver::report() method.
  * \param[in] fp File pointed passed by caller where the output is written to.
  * \param[in] details If >0 then driver details are printed.
  */
void EmuDaqSim::report(FILE *fp, int details)
{
    int deviceType;
    int APPSinature;

    fprintf(fp, "IOxOS IFC1410    : %s\n", this->portName);
    getIntegerParam(mDaqAPPSinature, &APPSinature);
    getIntegerParam(mDaqDeviceType, &deviceType);
    fprintf(fp,
            "Device type      : %X\n"
            "Firmware version : 0x%4X\n",
            deviceType,
            APPSinature);
    if (details > 0) {
        int numSamples, dataType;
        getIntegerParam(mDaqNumSamples, &numSamples);
        getIntegerParam(NDDataType, &dataType);
        fprintf(fp, "  # samples:       %d\n", numSamples);
        fprintf(fp, "      Data type:   %d\n", dataType);
    }
    /* Invoke the base class method */
    asynNDArrayDriver::report(fp, details);
}


/*********************************************************************************************************************/

/** Configuration command, called directly or from iocsh */
extern "C" int emudaqsimConfig(const char *portName, const int maxChannels,
        int numSamples, int extraPorts, int maxBuffers, int maxMemory,
        int priority, int stackSize)
{
    new EmuDaqSim(portName, maxChannels,
            numSamples, extraPorts,
            (maxBuffers < 0) ? 0 : maxBuffers,
            (maxMemory < 0) ? 0 : maxMemory,
            priority, stackSize);
    return(asynSuccess);
}

/** Code for iocsh registration */
static const iocshArg arg0 = {"Port name",     iocshArgString};
static const iocshArg arg1 = {"Max Channels",  iocshArgInt};
static const iocshArg arg2 = {"Num samples",   iocshArgInt};
static const iocshArg arg3 = {"Extra ports",   iocshArgInt};
static const iocshArg arg4 = {"maxBuffers",    iocshArgInt};
static const iocshArg arg5 = {"maxMemory",     iocshArgInt};
static const iocshArg arg6 = {"priority",      iocshArgInt};
static const iocshArg arg7 = {"stackSize",     iocshArgInt};
static const iocshArg * const configArgs[] = {
    &arg0, &arg1, &arg2, &arg3, &arg4, &arg5, &arg6, &arg7};
static const iocshFuncDef config = {"emudaqsimConfig", 8, configArgs};

static void configCallFunc(const iocshArgBuf *args)
{
    emudaqsimConfig(args[0].sval, args[1].ival, args[2].ival,
        args[3].ival, args[4].ival, args[5].ival, args[6].ival, args[7].ival);
}

static void emudaqsimRegister(void)
{
    iocshRegister(&config, configCallFunc);
}

extern "C" {
    epicsExportRegistrar(emudaqsimRegister);
}
