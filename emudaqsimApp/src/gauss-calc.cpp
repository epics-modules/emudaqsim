#include <stdio.h>
#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <epicsTypes.h>
#include <cmath>
#include <cstring>
#include <string.h> // Provides memcpy prototype
#include <stdlib.h> // Provides calloc prototype
#include <ctgmath>
#include <iostream>

static long gaussCalc(aSubRecord *prec)
{
    // initial parameters
    double k_peak = *(double *)prec->a;
    double k_center = *(double *)prec->b;
    double k_stddev = *(double *)prec->c; 
    int nelem = *(int *)prec->d; 

    if (k_stddev == 0.0) k_stddev = 0.001;

    // allocate the waveform 
    double *waveform = (double *) calloc(nelem, sizeof(double));

    // Gaussian function calculation
    for (size_t i = 0; i < (size_t)nelem; i++)
    {
        double a1 = pow(((double)i-((double)nelem/2.0) - k_center),2.0);
        const double b1 = 2.0*pow(k_stddev,2.0);
        const double expv = (a1/b1) * (-1);
        const double val = exp(expv) * k_peak;
        waveform[i] = val;
    }

    // Copy databuffer to output
    memcpy(prec->vala, waveform, nelem * sizeof(double));
    free(waveform);

    return 0;
}

extern "C"
{
    epicsRegisterFunction(gaussCalc);
}
