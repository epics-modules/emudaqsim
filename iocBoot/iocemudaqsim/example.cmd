#!../../bin/darwin-x86/emudaqsimIOC

epicsEnvSet("IOC_DIR", "/Users/joaopaulomartins/Develop/emudaqproj/emudaqsim/iocBoot/iocemudaqsim")
< $(IOC_DIR)/envPaths

## Register all support components
dbLoadDatabase "$(TOP)/dbd/emudaqsimIOC.dbd"
emudaqsimIOC_registerRecordDeviceDriver pdbbase
errlogInit(20000)
callbackSetQueueSize(15000)

#- load the instance definition
< $(IOC_DIR)/instance.iocsh

#- 10 MB max CA request
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")
epicsEnvSet("PREFIX",                       "$(CONTROL_GROUP):$(AMC_NAME):")
epicsEnvSet("PORT",                         "$(AMC_NAME)")
epicsEnvSet("MAX_SAMPLES",                  "100000")
#- AD plugin macros
epicsEnvSet("XSIZE",                        "$(MAX_SAMPLES)")
epicsEnvSet("YSIZE",                        "1")
epicsEnvSet("QSIZE",                        "20")
epicsEnvSet("NCHANS",                       "100")
epicsEnvSet("CBUFFS",                       "500")
epicsEnvSet("MAX_THREADS",                  "4")
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(ADCORE)/db")


#- Create a EMU DAQ SIM driver
#- int emudaqsimConfig(const char *portName, const int maxChannels,
#-         int numSamples, int extraPorts, int maxBuffers, int maxMemory,
#-         int priority, int stackSize)
emudaqsimConfig("$(PORT)", 24, $(MAX_SAMPLES), 0, 0)
dbLoadRecords("$(TOP)/db/emudaqsim.template","P=$(PREFIX),R=,PORT=$(PORT),MAX_SAMPLES=$(MAX_SAMPLES)")

# #- 24x data channels
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=0, NAME=CH1")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=1, NAME=CH2")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=2, NAME=CH3")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=3, NAME=CH4")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=4, NAME=CH5")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=5, NAME=CH6")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=6, NAME=CH7")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=7, NAME=CH8")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=8, NAME=CH9")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=9, NAME=CH10")

iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=10, NAME=CH11")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=11, NAME=CH12")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=12, NAME=CH13")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=13, NAME=CH14")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=14, NAME=CH15")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=15, NAME=CH16")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=16, NAME=CH17")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=17, NAME=CH18")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=18, NAME=CH19")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=19, NAME=CH20")

iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=20, NAME=CH21")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=21, NAME=CH22")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=22, NAME=CH23")
iocshLoad("$(IOC_DIR)/channel.iocsh", "ADDR=23, NAME=CH24")

#asynSetTraceIOMask("$(PORT)",0,2)
#asynSetTraceMask("$(PORT)",0,255)

#- Prepare autosave directories
epicsEnvSet("DB_DIR", "$(TOP)/db")
epicsEnvSet("AUTOSAVE_DIR", "./autosave")

#- Configure autosave directories
set_requestfile_path("$(DB_DIR)")
set_requestfile_path("./")
set_savefile_path("$(AUTOSAVE_DIR)")

#- apply default PV values (located in IOC_DIR)
# set_pass0_restoreFile("$(IOC_DIR)/default_settings.sav", "P=$(PREFIX),R=")
# set_pass1_restoreFile("$(IOC_DIR)/default_settings.sav", "P=$(PREFIX),R=")

#- apply runtime changed PV values (located in AUTOSAVE_DIR)
set_pass0_restoreFile("info_positions.sav")
set_pass1_restoreFile("info_settings.sav")

# save_restoreSet_status_prefix("$(PREFIX)")
dbLoadRecords("$(AUTOSAVE)/db/save_restoreStatus.db","P=$(PREFIX)")

###############################################################################
iocInit
###############################################################################

#- build info_positions.req file from record info 'autosaveFields_pass0' field
#- build info_settings.req file from record info 'autosaveFields' field
makeAutosaveFiles("")
create_monitor_set("info_positions.req", 5)
#- save things every thirty seconds
create_monitor_set("info_settings.req", 30)

date
###############################################################################
