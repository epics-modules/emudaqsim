EMU DAQ Simulator
=========

Software module that provides EPICS device support for a simulated N-channel digitizer. Based on the [ADSIS8300 EPICS module](https://gitlab.esss.lu.se/beam-diagnostics/bde/modules/adsis8300).

This module is to be used in the developments of  scanning applications for the ESS Emittance Meter Unit (EMU) instruments.

Dependencies
-------------

In order to build and use this modules, one needs to install the following packages:

- [EPICS Base](https://github.com/epics-base/epics-base)
- [asyn](https://github.com/epics-modules/asyn)
- [calc](https://github.com/epics-modules/calc)
- [autosave](https://github.com/epics-modules/autosave)
- [sscan](https://github.com/epics-modules/sscan)
- [busy](https://github.com/epics-modules/busy)
- [sequencer](https://www-csr.bessy.de/control/SoftDist/sequencer/)
- [areaDetector](https://github.com/areadetector)
- [admisc](https://gitlab.esss.lu.se/beam-diagnostics/bde/modules/admisc) - ESS module

